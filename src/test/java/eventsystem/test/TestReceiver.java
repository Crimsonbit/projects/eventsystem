package eventsystem.test;

import at.crimsonbit.cbes.annotations.SubscribeEvent;
import at.crimsonbit.cbes.event.Event;

public class TestReceiver {

	private String name;

	public TestReceiver(String name) {
		this.name = name;
	}

	@SubscribeEvent
	public void printString(TestEvent e) {
		System.out.println(name + ": " + e.str + " Calling class: " + e.getSource());
		
	}
}
