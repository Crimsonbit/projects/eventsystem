package eventsystem.test;

import at.crimsonbit.cbes.event.Event;

public class TestEvent extends Event{
	
	public String str;

	public TestEvent(Object source, String str) {
		super(source);
		this.str = str;
	}

}
