package eventsystem.test;

import at.crimsonbit.cbes.EventSystem;

public class EventTest {
	public EventTest() {
		TestReceiver rec = new TestReceiver("Rec1");
		TestReceiver rec2 = new TestReceiver("Rec2");

		EventSystem eventSystem = new EventSystem();

		eventSystem.register(rec);
		eventSystem.register(rec2);
		eventSystem.post(new TestEvent(this, "Hello World1"));
		eventSystem.post(new TestEvent(this, "Hello World1"));
		eventSystem.remove(rec);
		eventSystem.post(new TestEvent(this, "Hello World1"));
		eventSystem.post(new TestEvent(this, "Hello World2"));

		eventSystem.shutdown();
	}
}
