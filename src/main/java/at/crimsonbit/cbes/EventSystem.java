package at.crimsonbit.cbes;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import at.crimsonbit.cbes.annotations.SubscribeEvent;
import at.crimsonbit.cbes.dispatcher.DefaultEventDispatcher;
import at.crimsonbit.cbes.dispatcher.IEventDispatcher;
import at.crimsonbit.cbes.event.Event;
import at.crimsonbit.cbes.subscriber.IEventSubscriber;
import at.crimsonbit.cbes.subscriber.Subscriber;

/**
 * The EventSystem allows communication between multiple components without them
 * being aware of each other -> registring only to the EventSystem.<br>
 * <h2>Receiving Events</h2>
 * <ol>
 * <li>The event subscriber, the method which handles the event, is only allowed
 * to accept a single argument -> the event.
 * <li>Mark it with a {@link SubscribeEvent} annotation;
 * <li>Register the object to an EventSystem instance's using the
 * {@link #register(Object)} method.
 * </ol>
 * <br>
 * <h2>Posting Events</h2>
 * <p>
 * To post an event, simply provide the event object to the {@link #post(Event)}
 * method. The EventSystem will handle the routing of the event to the
 * corresponding subscriber.
 * <p>
 * <h2>Event Dispatchers</h2>
 * <p>
 * You can provide your own {@link IEventDispatcher} to create your own
 * dispatcher. The dispatcher can only be provided in the {@link #EventSystem()}
 * constructor and can not be changed afterwards.
 * <p>
 * 
 * @author Florian Wagner
 *
 */
public class EventSystem implements IEventSystem {

	private final IEventDispatcher dispatcher;
	private final Map<Class<?>, List<IEventSubscriber>> subscriber;
	private final String name;

	/**
	 * Creates a new EventSystem object. <br>
	 * The default name is 'global' and the used dispatcher is the
	 * {@link DefaultEventDispatcher}.
	 */
	public EventSystem() {
		this("global");
	}

	/**
	 * Creates a new EventSystem object. <br>
	 * It uses the given name as its name/identifier. The used dispatcher is the
	 * {@link DefaultEventDispatcher}.
	 * 
	 * @param name
	 *            the name / identifier to give this EventSystem.
	 */
	public EventSystem(String name) {
		this(name, new DefaultEventDispatcher());
	}

	/**
	 * Creates a new EventSystem object. <br>
	 * The default name is 'global'. It uses the given {@link IEventDispatcher} as
	 * its dispatcher.
	 * 
	 * @param dispatcher
	 *            the event dispatcher to use
	 */
	public EventSystem(IEventDispatcher dispatcher) {
		this("global", dispatcher);
	}

	/**
	 * Creates a new EventSystem object. <br>
	 * 
	 * @param name
	 *            the name / identifier to give this EventSystem.
	 * @param dispatcher
	 *            the dispatcher which should be used
	 */
	public EventSystem(String name, IEventDispatcher dispatcher) {
		this.name = name;
		this.dispatcher = dispatcher;
		this.subscriber = new ConcurrentHashMap<>();
	}

	/**
	 * {@inheritDoc}
	 */
	public void post(Event event) {
		if (event != null) {
			if (!event.isConsumed()) {
				Class<?> clazz = event.getClass();
				if (subscriber.containsKey(clazz)) {
					if (dispatcher != null) {
						subscriber.get(clazz).forEach(subscriber -> {
							dispatcher.dispatchEvent(subscriber, event);
						});
					}
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void register(Object object) {
		Class<?> currentClass = object.getClass();
		while (currentClass != null) {

			List<Method> subscribeMethods = findSubscriptionMethods(currentClass);
			for (Method method : subscribeMethods) {
				Class<?> type = method.getParameterTypes()[0];

				if (subscriber.containsKey(type)) {
					subscriber.get(type).add(new Subscriber(method, object));

				} else {
					List<IEventSubscriber> temp = new Vector<>();
					temp.add(new Subscriber(method, object));
					subscriber.put(type, temp);
				}
			}
			currentClass = currentClass.getSuperclass();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void remove(Object object) {
		Class<?> currentClass = object.getClass();
		if (currentClass != null) {
			Set<Class<?>> keys = subscriber.keySet();

			for (Class<?> cls : keys) {
				Iterator<IEventSubscriber> subs = subscriber.get(cls).iterator();

				while (subs.hasNext()) {
					if (subs.next().getTarget() == object) {
						subs.remove();
					}
				}
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void shutdown() {
		if (dispatcher != null)
			dispatcher.close();
	}

	/**
	 * @return the name / identiefier of this EventSystem.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return a list of all {@link IEventSubscriber} that are registered
	 */
	public Collection<List<IEventSubscriber>> getEventSubscriber() {
		return subscriber.values();
	}

	/**
	 * @return the {@link IEventDispatcher} used by this EventSystem
	 */
	public IEventDispatcher getDispatcher() {
		return dispatcher;
	}

	private List<Method> findSubscriptionMethods(Class<?> type) {
		List<Method> subscribeMethods = Arrays.stream(type.getDeclaredMethods())
				.filter(method -> method.isAnnotationPresent(SubscribeEvent.class)).collect(Collectors.toList());
		checkSubscriberMethods(subscribeMethods);
		return subscribeMethods;
	}

	private void checkSubscriberMethods(List<Method> subscribeMethods) {
		boolean hasMoreThanOneParameter = subscribeMethods.stream().anyMatch(method -> method.getParameterCount() != 1);
		if (hasMoreThanOneParameter) {
			throw new IllegalArgumentException("Method annotated with @SusbscribeEvent has more than one parameter");
		}
	}
}