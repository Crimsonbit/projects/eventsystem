package at.crimsonbit.cbes.dispatcher;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import at.crimsonbit.cbes.EventSystem;
import at.crimsonbit.cbes.event.Event;
import at.crimsonbit.cbes.subscriber.IEventSubscriber;

/**
 * The {@link DefaultEventDispatcher} which is used by the {@link EventSystem}.
 * <br>
 * It uses a single thread {@link Executor} hence a {@link ExecutorService}.
 * 
 * @author Florian Wagner
 *
 */
public class DefaultEventDispatcher implements IEventDispatcher {

	private static final ExecutorService executorService = Executors.newSingleThreadExecutor();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispatchEvent(final IEventSubscriber subscriber, final Event event) {
		executorService.execute(new Runnable() {
			public void run() {
				subscriber.invokeMethod(event);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public void close() {
		executorService.shutdown();
	}

}
