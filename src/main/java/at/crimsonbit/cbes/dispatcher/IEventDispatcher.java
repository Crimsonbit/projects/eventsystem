package at.crimsonbit.cbes.dispatcher;

import java.util.concurrent.Executor;

import at.crimsonbit.cbes.EventSystem;
import at.crimsonbit.cbes.IEventSystem;
import at.crimsonbit.cbes.event.Event;
import at.crimsonbit.cbes.subscriber.IEventSubscriber;

/**
 * The {@link IEventDispatcher} interface is used to create new event
 * dispatchers. <br>
 * The {@link DefaultEventDispatcher} uses a single thread {@link Executor} to
 * dispatch events. <br>
 * The {@link EventSystem} can only use one {@link IEventDispatcher}.
 * 
 * @author Florian Wagner
 *
 */
public interface IEventDispatcher {

	/**
	 * This method is called to dispatch the event to the subscribers.
	 * 
	 * @param event
	 */
	public void dispatchEvent(IEventSubscriber subscriber, final Event event);

	/**
	 * This method is called to close and/or free any resources or threads if
	 * needed.<br>
	 * It is called uppon {@link IEventSystem#shutdown()}.
	 */
	public void close();

}
