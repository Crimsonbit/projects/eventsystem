package at.crimsonbit.cbes.subscriber;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import at.crimsonbit.cbes.event.Event;

/**
 *
 * @author Florian Wagner
 *
 */
public class Subscriber implements IEventSubscriber {

	private final Object target;
	private final Method method;

	public Subscriber(Method method, Object target) {
		this.target = target;
		this.method = method;
		this.method.setAccessible(true);
	}

	/**
	 * {@inheritDoc}
	 */

	public void invokeMethod(Event event) {
		try {
			getMethod().invoke(getTarget(), event);
		} catch (IllegalArgumentException e) {
			throw new Error("Method rejected target/argument: " + event, e);

		} catch (IllegalAccessException e) {
			throw new Error("Method became inaccessible: " + event, e);

		} catch (InvocationTargetException e) {

			if (e.getCause() instanceof Error) {
				throw (Error) e.getCause();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Object getTarget() {
		return target;
	}

	/**
	 * {@inheritDoc}
	 */
	public Method getMethod() {
		return method;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final int hashCode() {
		return (31 + method.hashCode()) * 31 + System.identityHashCode(target);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final boolean equals(Object obj) {
		if (obj instanceof Subscriber) {
			Subscriber that = (Subscriber) obj;
			return target == that.target && method.equals(that.method);
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "Subscriber [target: " + target + ", method: " + method + "]";
	}
}
