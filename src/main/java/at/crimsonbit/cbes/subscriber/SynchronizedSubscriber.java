package at.crimsonbit.cbes.subscriber;

import java.lang.reflect.Method;

import at.crimsonbit.cbes.event.Event;

/**
 * 
 * @author Florian Wagner
 *
 */
public class SynchronizedSubscriber extends Subscriber {
	
	private SynchronizedSubscriber(Method method, Event target) {
		super(method, target);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void invokeMethod(Event event) {
		synchronized (this) {
			super.invokeMethod(event);
		}
	}
}
