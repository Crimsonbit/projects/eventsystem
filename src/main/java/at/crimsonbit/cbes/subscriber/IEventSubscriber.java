package at.crimsonbit.cbes.subscriber;

import java.lang.reflect.Method;

import at.crimsonbit.cbes.event.Event;

/**
 * 
 * @author Florian Wagner
 *
 */
public interface IEventSubscriber {

	/**
	 * Invokes the given method of the event subscriber that.
	 * 
	 * @param event
	 *            the event to invoke
	 */
	public void invokeMethod(Event event);

	/**
	 * This method returns the target object of this event subscriber.
	 * 
	 * @return the target object
	 */
	public Object getTarget();

	/**
	 * This method returns the {@link Method} which should be called uppon
	 * invokation.
	 * 
	 * @return the method to invoke if this subscriber event is being post
	 */
	public Method getMethod();

}
