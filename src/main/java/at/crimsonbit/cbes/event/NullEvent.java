package at.crimsonbit.cbes.event;

public class NullEvent extends Event {

	private static final long serialVersionUID = -6230631726018481535L;

	public NullEvent(Object source) {
		super(source);
	}

}
