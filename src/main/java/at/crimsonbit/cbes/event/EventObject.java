package at.crimsonbit.cbes.event;

import java.io.Serializable;

public abstract class EventObject implements Serializable {

	private static final long serialVersionUID = -1057270862766026087L;
	protected transient Object source;

	public EventObject(Object source) {
		this.source = source;
	}
	
	public EventObject() {
	}

	public Object getSource() {
		return source;
	}

	public String toString() {
		return getClass().getName() + "[source=" + source + "]";
	}
}