package at.crimsonbit.cbes.event;

import java.io.IOException;

public class Event extends EventObject implements Cloneable {

	public Event(Object source) {
		this.source = source;
	}

	private static final long serialVersionUID = -6430104819975829517L;
	protected boolean consumed;

	public Event copyFor(final Object newSource) {
		final Event newEvent = (Event) clone();
		newEvent.source = (newSource != null) ? newSource : source;
		newEvent.consumed = false;
		return newEvent;
	}

	public boolean isConsumed() {
		return consumed;
	}

	public void consume() {
		consumed = true;
	}

	@Override
	public Object clone() {
		try {
			return super.clone();
		} catch (final CloneNotSupportedException e) {
			throw new RuntimeException("Can't clone Event");
		}
	}

	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		source = null;
	}
}