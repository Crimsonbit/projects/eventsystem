package at.crimsonbit.cbes;

import at.crimsonbit.cbes.annotations.SubscribeEvent;
import at.crimsonbit.cbes.dispatcher.IEventDispatcher;
import at.crimsonbit.cbes.event.Event;

/**
 * * The EventSystem allows communication between multiple components without
 * them being aware of each other -> registring only to the EventSystem.<br>
 * 
 * @author Florian Wagner
 *
 */
public interface IEventSystem {

	/**
	 * Posts the given event to all subscribed subscribers.
	 * 
	 * @param event
	 *            the event to send
	 */
	public void post(Event event);

	/**
	 * Registeres a new object to the subsriber list. All Methods of this object
	 * containing the {@link SubscribeEvent} annotation will be registered.
	 * 
	 * @param object
	 *            the object to register
	 */
	public void register(Object object);

	/**
	 * Removes the given object from the subscriber list if it exists.
	 * 
	 * @param object
	 *            the object to remove from the subscription list
	 */
	public void remove(Object object);

	/**
	 * This method should be called when you are closing the program to clean up any
	 * resources. Mostly this is used for the internal {@link IEventDispatcher} to
	 * shut-down.
	 */
	public void shutdown();
}
